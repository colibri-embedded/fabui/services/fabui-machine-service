#ifndef MACHINE_SERVICE_HPP
#define MACHINE_SERVICE_HPP

#include <algorithm>
#include <chrono>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <fabui/wamp/session.hpp>

namespace YAML {
	class Node;
}


namespace fabui {

class MachineService {
	public:
		/// Create a MachineService
		MachineService(const std::string& machine_file);

		/// Cleanup MachineService
		~MachineService();
		
		/// Stop service placeholder
		void stop();

		/**
		 * Get machine info.
		 *
		 * @returns Machine info as a json object
		 */
		wampcc::json_object info();

		/**
		 * Reset machine controller.
		 *
		 * @returns true on succcess, false otherwise
		 */
		bool controllerReset();

		/**
		 * Run machine macro.
		 *
		 * @param macro_name Macro name
		 * @param env Macro environment
		 * @returns true on success, false otherwise
		 */
		bool macroRun(const std::string& macro_name, const wampcc::json_object& env = {});

		/**
		 * Set wamp session proxy.
		 *
		 * @param session WampSession
		 */
		void setWampSession(IWampSession* session);

		/**
		 * Get wamp proxy.
		 *
		 * @return Wamp Proxy
		 */
		IWampSession* getWampSession();

	private:
		IWampSession *m_session;

		/// All machine specification values
		std::map<std::string, std::string> m_spec;
		/// Reset state duration in milliseconds
		unsigned m_reset_duration_ms;

	protected:

		bool parseNode(const YAML::Node& node, const std::string& root = "");

		/**
		 * Load machine specs from a file.
		 *
		 * @param filename Specs filename
		 * @returns true on success, false otherwise
		 */
		bool loadSpecs(const std::string& filename);

		bool haveSetting(const std::string& key);
};

} // namespace fabui

#endif /* MACHINE_SERVICE_HPP */
